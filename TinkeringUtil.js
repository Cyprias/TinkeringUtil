/********************************************************************************************\
	File Name:      TinkeringUtil.js
	Purpose:        Provide various functions related to tinkering.
	Creator:        Cyprias
	Date:           04/20/2015
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Version:        1.2
\*********************************************************************************************/

var MAJOR = "TinkeringUtil-1.2";
var MINOR = 190108; // year month day

(function (factory) {
	// Check if script was loaded via require().
    if (typeof module === 'object' && module.exports) {
        module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
        TinkeringUtil12 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	
	core.debugging = false;
	var materials = core.materials = {};

	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	

	core.console = function fConsole(szMsg) {
		var args = Array.prototype.slice.call(arguments);
		var d = new Date();
		var datestring = ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2)  + ":" + ("0" + d.getSeconds()).slice(-2) + "." + ("00" + d.getMilliseconds()).slice(-3); 
		skapi.OutputLine(datestring + " [" + core.MAJOR + "] " + args.join(', '), opmConsole);
	};
	
	core.debug = function debug(szMsg) {
		/******************************************************************************\
			debug: Print a message to chat.
		\******************************************************************************/
		if (core.debugging == true) {
			var args = Array.prototype.slice.call(arguments);
			core.console("[D] " + args.join(', '));
		}
	};
	
	function getNeededTinksAddition(value, target) {
		debug("<getNeededTinksAddition> value: " + value + ", target: " + target);
		var r = Math.max(0, Math.ceil(round(((target - value)/this.modAmount),1)));// Rounding to 1 cause we tend to endup with long floating numbers. 9.999999992 or 10.000000004.
		debug("r: " + r);
		return r;
	}
	
	function getNeededTinksSubtraction(value, target) {
		debug("<getNeededTinksSubtraction> value: " + value + ", target: " + target);
		var r = Math.max(0, Math.floor(round((value - target)/this.modAmount,1)));// Rounding to 1 cause we tend to endup with long floating numbers. 9.999999992 or 10.000000004.
		debug("r: " + r);
		return r;
	};
	
	function getTinkedValueAddition(value, tinks) {
		if (typeof tinks === "undefined") {
			tinks = 1;
		}
		if (tinks == 0)  {
			return value;
		} else {
			return this.getTinkedValue(value + this.modAmount, tinks - 1);
		}
	};
	
	function getTinkedValueSubtraction(value, tinks) {
		if (typeof tinks === "undefined") {
			tinks = 1;
		}
		if (tinks == 0)  {
			return value;
		} else {
			return this.getTinkedValue(value - this.modAmount, tinks - 1);
		}
	};
	
	function getTinkedValueMultiplication(value, tinks) {
		if (typeof tinks === "undefined") {
			tinks = 1;
		}
		if (tinks == 0)  {
			return value;
		} else {
			return this.getTinkedValue(value * this.modAmount, tinks - 1);
		}
	};
	
	function getNeededTinksMultiplication(value, target) {
		debug("<getNeededTinksMultiplication> value: " + value + ", target: " + target + ", modAmount: " + this.modAmount);
		var v = target/value;
		debug("v: " + v);
		var v2 = Math.log(v)/Math.log(this.modAmount);
		debug("v2: " + v2);
		var r = Math.max(0, Math.ceil(v2));
		debug("r: " + r);
		return r;
	};

	function round(rnum, rlength) { // Arguments: number to round, number of decimal places
		if (rlength == null) rlength = 0;
		return Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
	};
	
	function getMaterialChance( params ) {
		var skillCurrent = params.skillCurrent;
		var skillNeeded = params.skillNeeded;
		var material = params.material;
		//var X = matMod[material];

		var Chance = (1 - (1 / (1 + Math.exp( 0.03 * (skillCurrent - skillNeeded))))) * 100;
		switch(material) {					// roundoff to 2 decimal points Chance or 1/3 of Chance depending on the the material
			case materialPeridot: Chance /= 3; break;	// Peridot
			case materialYellowTopaz: Chance /= 3; break;	// Yellow Topaz
			case materialZircon: Chance /= 3; break;	// Zircon
			case materialAquamarine: Chance /= 3; break;	// Aquamarine
			case materialBlackGarnet: Chance /= 3; break;	// Black Garnet
			case materialEmerald: Chance /= 3; break;	// Emerald
			case materialImperialTopaz: Chance /= 3; break;	// Imperial Topaz
			case materialJet: Chance /= 3; break;	// Jet
			case materialRedGarnet: Chance /= 3; break;	// Red Garnet
			case materialWhiteSapphire: Chance /= 3; break;	// White Sapphire
			case materialAgate: Chance /= 3; break;	// Agate
			case materialAzurite: Chance /= 3; break;	// Azurite
			case materialBlackOpal: Chance /= 3; break;	// Black Opal
			case materialBloodstone: Chance /= 3; break;	// Blood Stone
			case materialCarnelian: Chance /= 3; break;	// Carnelia
			case materialCitrine: Chance /= 3; break;	// Citrine
			case materialFireOpal: Chance /= 3; break;	// Fire Opal
			case materialHematite: Chance /= 3; break;	// Hematite
			case materialLavenderJade: Chance /= 3; break;	// Lavender Jade
			case materialMalachite: Chance /= 3; break;	// Malachite
			case materialRedJade: Chance /= 3; break;	// Red Jade
			case materialRoseQuartz: Chance /= 3; break;	// Rose Quartz
			case materialSunstone: Chance /= 3; break;	// Sunstone
		}
		//debug("Success: " + Round( Chance, 2 ) + "%");
		return Chance;
	};
	
	var attempts = [0,1,1.1,1.3,1.6,2,2.5,3,3.5,4,4.5];//Attempt skill modifier
		
	/**
	 * getSkillNeeded() Returns skill needed to land a tink.
	 */
	core.getSkillNeeded = function getSkillNeeded( params ) {
		var itemWorkmanship = params.itemWorkmanship;
		var material = params.material;
		var salvageWorkmanship = params.salvageWorkmanship;
		var attempt = params.attempt;

		var chanceMod = materials[material].chanceMod;
		attempt = attempt || 1;
		
		core.debug("<getSkillNeeded>", "itemWorkmanship: " + itemWorkmanship, "material: " + material, "salvageWorkmanship: " + salvageWorkmanship, "attempt: " + attempt, "chanceMod: " + chanceMod);
		
		if (!attempts[attempt]) {
			debug("Can't tink item anymore.");
			return;
		}

		var aMod = attempts[attempt];// attempt mod;
		
		core.debug("aMod: " + aMod);
		
		if (isNaN(chanceMod)) {
			debug("The base modifier for the material you have selected has not yet been calculated");
			return;
		} else if (!chanceMod) { // Item has no difficulty
			salvageWorkmanship = 0;
			aMod = 0;
		} else if (!salvageWorkmanship) {
			debug("Enter salvaged material quality");
			return;
		} else if (salvageWorkmanship > 10 || salvageWorkmanship < 0) {
			debug("Salvaged material quality must be between 0 and 10");
			return;
		}
		
		var M = (salvageWorkmanship < itemWorkmanship) ? 1 : 2;
		
		core.debug("M: " + M);
		
		return Math.floor(((5 * chanceMod) + (2 * itemWorkmanship * chanceMod) - (salvageWorkmanship * M * chanceMod / 5)) * aMod);
	};
	

	/**
	 * getTinkChance() Returns chance percentage of landing a tink.
	 */
	core.getTinkChance = function getTinkChance( params ) {
		var itemWorkmanship = params.itemWorkmanship;
		var material = params.material;
		var salvageWorkmanship = params.salvageWorkmanship;
		var skillCurrent = params.skillCurrent;
		var attempt = params.attempt;

		core.debug("<getTinkChance>", "itemWorkmanship: " + itemWorkmanship, "material: " + material, "salvageWorkmanship: " + salvageWorkmanship, "skillCurrent: " + skillCurrent, "attempt: " + attempt);

		var skillNeeded = core.getSkillNeeded({
			itemWorkmanship: itemWorkmanship,
			material: material,
			salvageWorkmanship: salvageWorkmanship,
			attempt: attempt
		});
		
		core.debug("skillNeeded: " + skillNeeded);
		
		return round(getMaterialChance({
			skillCurrent:skillCurrent,
			skillNeeded: skillNeeded,
			material: material
		}),2);
	};
	
	var Material;
	(function(){
		Material = function(skid, materialId, name, description, modAmount, needAssessment, getNeededTinks, getTinkedValue, chanceMod) {
			this.skid               = skid;
			this.material           = materialId;
			this.name               = name;
			this.description        = description;
			this.modAmount          = modAmount;
			this.needAssessment		= needAssessment;
			this.getNeededTinks       = getNeededTinks;
			this.getTinkedValue       = getTinkedValue;
			// https://www.asheronscall.com/en/forums/showthread.php?54727-Maybe-I-shouldn-t-complain-but-did-tinkering-just-get-easier
			// Seems all chanceMod is 10 now. 
			this.chanceMod          = 10;//chanceMod;
		};
		
		Material.prototype.getThisSkillNeeded = function getThisSkillNeeded( params ) {
			var itemWorkmanship = params.itemWorkmanship;
			var salvageWorkmanship = params.salvageWorkmanship
			var attempt = params.attempt;

			return getSkillNeeded({
				itemWorkmanship: itemWorkmanship,
				material: this.material,
				salvageWorkmanship: salvageWorkmanship,
				attempt: attempt
			});
		};
		
		Material.prototype.getThisTinkChance = function fGetThisTinkChance( params ) {
			var itemWorkmanship = params.itemWorkmanship;
			var salvageWorkmanship = params.salvageWorkmanship;
			var skillCurrent = params.skillCurrent;
			var attempt = params.attempt;

			core.debug("<getThisTinkChance>","itemWorkmanship: " + itemWorkmanship, "salvageWorkmanship: " + salvageWorkmanship, "skillCurrent: " + skillCurrent, "attempt: " + attempt);

			return core.getTinkChance({
				itemWorkmanship: itemWorkmanship,
				material: this.material,
				salvageWorkmanship: salvageWorkmanship,
				skillCurrent: skillCurrent,
				attempt: attempt
			});
		};
	})();

	materials[materialAlabaster] = new Material(skidArmorTinkering, materialAlabaster, "Alabaster", " Increases Armor's Piercing Protection by 0.2.", 0.2, true, getNeededTinksAddition, getTinkedValueAddition, 11);
	materials[materialAlabaster].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iai.protPiercing");
	};
	
	materials[materialBronze] = new Material(skidArmorTinkering, materialBronze, "Bronze", "Increases Armor's Slashing Protection by 0.2.", 0.2, true, getNeededTinksAddition, getTinkedValueAddition, 11);
	materials[materialBronze].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iai.protSlashing");
	};

	materials[materialCeramic] = new Material(skidArmorTinkering, materialCeramic, "Ceramic", "Increases Armor's Fire Protection by 0.4.", 0.4, true, getNeededTinksAddition, getTinkedValueAddition, 11);
	materials[materialCeramic].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iai.protFire");
	};
	
	materials[materialDilloHide] = new Material(skidArmorTinkering, materialDilloHide, "Armoredillo Hide", "Increases Armor's Acid Protection by 0.4.", 0.4, true, getNeededTinksAddition, getTinkedValueAddition, 11);
	materials[materialDilloHide].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iai.protAcid");
	};

	materials[materialMarble] = new Material(skidArmorTinkering, materialMarble, "Marble", "Increases Armor's Bludgeon Protection by 0.4.", 0.4, true, getNeededTinksAddition, getTinkedValueAddition, 11);
	materials[materialMarble].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iai.protBludgeoning");
	};
	
	materials[materialPeridot] = new Material(skidArmorTinkering, materialPeridot, "Peridot", "Imbues the target with a +1 bonus to Melee Defense.", null, false, null, null, 11);

	materials[materialReedsharkHide] = new Material(skidArmorTinkering, materialReedsharkHide, "Reedshark Hide", "Increases Armor's Lightning Protection by 0.4.", 0.4, true, getNeededTinksAddition, getTinkedValueAddition, 11);
	materials[materialReedsharkHide].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iai.protElectrical");
	};

	materials[materialSteel] = new Material(skidArmorTinkering, materialSteel, "Steel", "Increases Armor's Armor Level by +20.", 20, true, getNeededTinksAddition, getTinkedValueAddition, 12);
	materials[materialSteel].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iai.al");
	};

	materials[materialWool] = new Material(skidArmorTinkering, materialWool, "Wool", "Increases Armor's Cold Protection by 0.4.", 0.4, true, getNeededTinksAddition, getTinkedValueAddition, 11);
	materials[materialWool].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iai.protCold");
	};
	
	materials[materialYellowTopaz] = new Material(skidArmorTinkering, materialYellowTopaz, "Yellow Topaz", "Imbues the target with a +1 bonus to Missile Defense.", null, false, null, null, 20);

	materials[materialZircon] = new Material(skidArmorTinkering, materialZircon, "Zircon", "Imbues the target with a +1 bonus to Magic Defense.", null, false, null, null, 20);
	
	//////////////////////
	// Item tinkering   //
	//////////////////////

	materials[materialAmber] = new Material(skidItemTinkering, materialAmber, "Amber", "Minor, Moderate or Major Augmented Stamina.", null, false, null, null, 0);

	materials[materialCopper] = new Material(skidItemTinkering, materialCopper, "Copper", "Changes Missile Defense requirement to Melee Defense requirement.", null, false, null, null, 15);

	materials[materialDiamond] = new Material(skidItemTinkering, materialDiamond, "Diamond", "Minor, Moderate or Major Augmented Damage.", null, false, null, null, 0);

	materials[materialEbony] = new Material(skidItemTinkering, materialEbony, "Ebony", "Changes the item's heritage requirement to Gharu'ndim.", null, false, null, null, 12);

	materials[materialGold] = new Material(skidItemTinkering, materialGold, "Gold", "Increases the item's value by 25%.", 1.25, false, getNeededTinksMultiplication, getTinkedValueMultiplication, 10);
	materials[materialGold].getAcoValue = function(aco) {
		return aco.cpyValue;
	};

	materials[materialGromnieHide] = new Material(skidItemTinkering, materialGromnieHide, "Gromnie Hide", "Minor, Moderate or Major Augmented Damage Reduction.", null, false, null, null, 0);

	materials[materialLinen] = new Material(skidItemTinkering, materialLinen, "Linen", "Reduces the item's burden by 25% .", 0.75, false, getNeededTinksMultiplication, getTinkedValueMultiplication, 11);
	materials[materialLinen].getAcoValue = function(aco) {
		return aco.burden;
	};

	materials[materialMoonstone] = new Material(skidItemTinkering, materialMoonstone, "Moonstone", "Increases the item's Maximum Mana by 500.", 500, false, getNeededTinksAddition, getTinkedValueAddition, 11);
	materials[materialMoonstone].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iei.manaMax");
	};
	
	materials[materialPine] = new Material(skidItemTinkering, materialPine, "Pine", "Reduces the item's value by 25%.", 0.75, false, getNeededTinksMultiplication, getTinkedValueMultiplication, 11);
	materials[materialPine].getAcoValue = function(aco) {
		return aco.cpyValue;
	};

	materials[materialPorcelain] = new Material(skidItemTinkering, materialPorcelain, "Porcelain", "Changes the item's heritage requirement to Sho.", null, false, null, null, 12);

	materials[materialPyreal] = new Material(skidItemTinkering, materialPyreal, "Pyreal", "Minor, Moderate or Major Augmented Understanding.", null, false, null, null, 0);

	materials[materialRuby] = new Material(skidItemTinkering, materialRuby, "Ruby", "Minor, Moderate or Major Augmented Health.", null, false, null, null, 0);

	materials[materialSapphire] = new Material(skidItemTinkering, materialSapphire, "Sapphire", "Minor, Moderate or Major Augmented Mana.", null, false, null, null, 20);

	materials[materialSatin] = new Material(skidItemTinkering, materialSatin, "Satin", "Changes the item's heritage requirement to Viamontian.", null, false, null, null, 0);

	materials[materialSilver] = new Material(skidItemTinkering, materialSilver, "Silver", "Changes Melee Defense requirement to Missile Defense requirement.", null, false, null, null, 15);

	materials[materialTeak] = new Material(skidItemTinkering, materialTeak, "Teak", "Changes heritage requirement to Aluvian.", null, false, null, null, 12);
	
	//////////////////////////
	// Magic Item tinkering //
	//////////////////////////

	materials[materialAgate] = new Material(skidMagicItemTinkering, materialAgate, "Agate", "Imbues the target with Minor Focus.", null, false, null, null, 20);

	materials[materialAzurite] = new Material(skidMagicItemTinkering, materialAzurite, "Azurite", "Imbues the target with Wizard's Intellect.", null, false, null, null, 20);
	
	materials[materialBlackOpal] = new Material(skidMagicItemTinkering, materialBlackOpal, "Black Opal", "Imbues the target with Critical Strike.", null, false, null, null, 20);

	materials[materialBloodstone] = new Material(skidMagicItemTinkering, materialBloodstone, "Bloodstone", "Imbues the target with Minor Endurance.", null, false, null, null, 25);

	materials[materialCarnelian] = new Material(skidMagicItemTinkering, materialCarnelian, "Carnelian", "Imbues the target with Minor Strength.", null, false, null, null, 25);
	
	materials[materialCitrine] = new Material(skidMagicItemTinkering, materialCitrine, "Citrine", "Imbues the target with Minor Stamina Gain.", null, false, null, null, 25);

	materials[materialFireOpal] = new Material(skidMagicItemTinkering, materialFireOpal, "Fire Opal", "Imbues the target with Crippling Blow.", null, false, null, null, 20);

	function getPvMElemBonus(aco) {
		var scalePvMElemBonus = getObjectValue(aco, "oai.iwi.scalePvMElemBonus");
		if (!scalePvMElemBonus) return;

		var spells = getAcoSpells({aco:aco});
		var spellid, szName;
		for (var i = 0; i < spells.length; i++) {
			spellid = spells[i];
			szName = getSpellName(spellid);
			if (szName.match("Legendary Spirit Thirst")) {
				scalePvMElemBonus += 0.07;
				break;
			} else if (szName.match("Epic Spirit Thirst")) {
				scalePvMElemBonus += 0.05; // wiki says 4%, in game says 5%.
				break;
			} else if (szName.match("Major Spirit Thirst")) {
				scalePvMElemBonus += 0.03;
				break;
			} else if (szName.match("Minor Spirit Thirst")) {
				scalePvMElemBonus += 0.01;
				break;
			}
		}

		return scalePvMElemBonus;
	};
	
	materials[materialGreenGarnet] = new Material(skidMagicItemTinkering, materialGreenGarnet, "Green Garnet", "Improves a wand's PvM and PvP damage modifier by 1%.", 0.01, true, getNeededTinksAddition, getTinkedValueAddition, 0);
	materials[materialGreenGarnet].getAcoValue = function(aco) {
	//	return aco.oai && aco.oai.iwi && aco.oai.iwi.scalePvMElemBonus; // PvM instead of PvP because 0.01% incriments is simpler than 0.0025. 
		return getPvMElemBonus(aco);
	};

	materials[materialHematite] = new Material(skidMagicItemTinkering, materialHematite, "Hematite", "Imbues the target with Warrior's Vitality.", null, false, null, null, 25);

	materials[materialLapisLazuli] = new Material(skidMagicItemTinkering, materialLapisLazuli, "Lapis Lazuli", "Imbues the target with Minor Willpower.", null, false, null, null, 0);

	materials[materialLavenderJade] = new Material(skidMagicItemTinkering, materialLavenderJade, "Lavender Jade", "Imbues the target with Minor Mana Gain.", null, false, null, null, 25);

	materials[materialMalachite] = new Material(skidMagicItemTinkering, materialMalachite, "Malachite", "Imbues the target with Warrior's Vigor.", null, false, null, null, 25);
	
	materials[materialOpal] = new Material(skidMagicItemTinkering, materialOpal, "Opal", "Increases the item's Mana Conversion bonus by 1%.", 0.01, true, getNeededTinksAddition, getTinkedValueAddition, 11);
	materials[materialOpal].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.ibi.fractManaConvMod");
	};
	
	materials[materialRedJade] = new Material(skidMagicItemTinkering, materialRedJade, "Red Jade", "Imbues the target with Minor Health Gain.", null, false, null, null, 25);

	materials[materialRoseQuartz] = new Material(skidMagicItemTinkering, materialRoseQuartz, "Rose Quartz", "Imbues the target with Minor Quickness.", null, false, null, null, 25);

	materials[materialSmokeyQuartz] = new Material(skidMagicItemTinkering, materialSmokeyQuartz, "Smokey Quartz", "Imbues the target with Minor Coordination.", null, false, null, null, 0);

	materials[materialSunstone] = new Material(skidMagicItemTinkering, materialSunstone, "Sunstone", "Imbues the target with Armor Rending.", null, false, null, null, 20);

	//////////////////////
	// Weapon tinkering //
	//////////////////////

	materials[materialAquamarine] = new Material(skidWeaponTinkering, materialAquamarine, "Aquamarine", "Imbues the target with Cold Rending.", null, false, null, null, 20);

	materials[materialBlackGarnet] = new Material(skidWeaponTinkering, materialBlackGarnet, "Black Garnet", "Imbues the target with Pierce Rending.", null, false, null, null, 20);
	materials[materialBrass] = new Material(skidWeaponTinkering, materialBrass, "Brass", "Increases melee defense bonus by 1%.", 0.01, true, getNeededTinksAddition, getTinkedValueAddition, 11);//getNeededTinksMultiplication, getTinkedValueMultiplication
	
	var spellNames = {};
	function getSpellName(spellId) {
		if (!spellNames[spellId]) {
			spellNames[spellId] = skapi.SpellInfoFromSpellid(spellId).szName;
		}
		return spellNames[spellId];
	};
		
	function getDefenseBonus(aco) {
		var scaleDefenseBonus = getObjectValue(aco, "oai.iwi.scaleDefenseBonus");
		if (!scaleDefenseBonus) return;
		
		var spells = getAcoSpells({aco:aco});
		var spellid, szName;
		for (var i = 0; i < spells.length; i++) {
			spellid = spells[i];
			szName = getSpellName(spellid);
			if (szName.match("Legendary Defender")) {
				scaleDefenseBonus += 0.09;
				break;
			} else if (szName.match("Epic Defender")) {
				scaleDefenseBonus += 0.07;
				break;
			} else if (szName.match("Major Defender")) {
				scaleDefenseBonus += 0.05;
				break;
			} else if (szName.match("Minor Defender")) {
				scaleDefenseBonus += 0.03;
				break;
			}
		}
		return scaleDefenseBonus;
	};
	
	materials[materialBrass].getAcoValue = function(aco) {
		return getDefenseBonus(aco);
	};

	materials[materialEmerald] = new Material(skidWeaponTinkering, materialEmerald, "Emerald", "Imbues the target with Acid Rending.", null, false, null, null, 20);

	materials[materialGranite] = new Material(skidWeaponTinkering, materialGranite, "Granite", "Improves a melee weapon's variance by 20%.", 0.8, true, getNeededTinksMultiplication, getTinkedValueMultiplication, 11);
	materials[materialGranite].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iwi.scaleDamageRange");
	};

	materials[materialImperialTopaz] = new Material(skidWeaponTinkering, materialImperialTopaz, "Imperial Topaz", "Imbues the target with Slash Rending.", null, false, null, null, 20);

	function getNeededIronTinks(value, target) {
		return target - value;
	};
	
	function getDHealth(aco) {
		var dhealth = getObjectValue(aco, "oai.iwi.dhealth");
		if (!dhealth) return;

		var spells = getAcoSpells({aco:aco});
		var spellid, szName;
		for (var i = 0; i < spells.length; i++) {
			spellid = spells[i];
			szName = getSpellName(spellid);
			if (szName.match("Legendary Blood Thirst")) {
				dhealth += 10;
				break;
			} else if (szName.match("Epic Blood Thirst")) {
				dhealth += 7;
				break;
			} else if (szName.match("Major Blood Thirst")) {
				dhealth += 4;
				break;
			} else if (szName.match("Minor Blood Thirst")) {
				dhealth += 2;
				break;
			}
		}
		return dhealth;
	};
	
	materials[materialIron] = new Material(skidWeaponTinkering, materialIron, "Iron", "Increases the weapon's maximum damage by 1.", 1, true, getNeededIronTinks, getTinkedValueAddition, 12);
	materials[materialIron].getAcoValue = function(aco) {
		return getDHealth(aco);
	};

	materials[materialJet] = new Material(skidWeaponTinkering, materialJet, "Jet", "Imbues the target with Lightning Rending.", null, false, null, null, 20);

	materials[materialMahogany] = new Material(skidWeaponTinkering, materialMahogany, "Mahogany", "Increases the weapon's damage modifier by 4%.", 1.04, true, getNeededTinksMultiplication, getTinkedValueMultiplication, 12);
	materials[materialMahogany].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iwi.scaleDamageBonus");
	};

	materials[materialOak] = new Material(skidWeaponTinkering, materialOak, "Oak", "Decreases the weapon's speed by 50.", 50, true, getNeededTinksSubtraction, getTinkedValueSubtraction, 10);
	materials[materialOak].getAcoValue = function(aco) {
		return getObjectValue(aco, "oai.iwi.speed");
	};

	materials[materialRedGarnet] = new Material(skidWeaponTinkering, materialRedGarnet, "Red Garnet", "Imbue the target with Fire Rending", null, false, null, null, 20);

	function getAttackBonus(aco) {
		var scaleAttackBonus = getObjectValue(aco, "oai.iwi.scaleAttackBonus");
		if (!scaleAttackBonus) return;
		
		var spells = getAcoSpells({aco:aco});
		var spellid, szName;
		for (var i = 0; i < spells.length; i++) {
			spellid = spells[i];
			szName = getSpellName(spellid);
			if (szName.match("Legendary Heart Thirst")) {
				scaleAttackBonus += 0.09;
				break;
			} else if (szName.match("Epic Heart Thirst")) {
				scaleAttackBonus += 0.07;
				break;
			} else if (szName.match("Major Heart Thirst")) {
				scaleAttackBonus += 0.05;
				break;
			} else if (szName.match("Minor Heart Thirst")) {
				scaleAttackBonus += 0.03;
				break;
			}
		}
		return scaleAttackBonus;
	};
	
	materials[materialVelvet] = new Material(skidWeaponTinkering, materialVelvet, "Velvet", "Increases the weapon's attack skill bonus by 1%.", 0.01, true, getNeededTinksAddition, getTinkedValueAddition, 11); //getNeededTinksMultiplication, getTinkedValueMultiplication
	materials[materialVelvet].getAcoValue = function(aco) {
		return getAttackBonus(aco);
	};

	materials[materialWhiteSapphire] = new Material(skidWeaponTinkering, materialWhiteSapphire, "White Sapphire", "Imbues the target with Bludgeon Rending.", null, false, null, null, 20);
	
	
	//////////////////////
	// Misc tinkering   //
	//////////////////////
	
	materials[materialIvory] = new Material(null, materialIvory, "Ivory", "Unattunes certain quest items from your soul.", null, false, null, null, 0);

	materials[materialLeather] = new Material(null, materialLeather, "Leather", "Renders an item Retained.", null, false, null, null, 0);

	materials[materialSandstone] = new Material(null, materialSandstone, "Sandstone", "Remove a items Retained status ", null, false, null, null, 0);

	materials[materialSilk] = new Material(null, materialSilk, "Silk", "Removes the rank activation requirement.", null, false, null, null, 25);
	
	//////////////////////
	// Unused tinkering //
	//////////////////////
	materials[materialAmethyst]         = new Material(null, materialAmethyst,          "Amethyst",         "", null, false, null, null, 0);
	materials[materialCloth]            = new Material(null, materialCloth,             "Cloth",            "", null, false, null, null, 0);
	materials[materialGem]              = new Material(null, materialGem,               "Gem",              "", null, false, null, null, 0);
	materials[materialGreenJade]        = new Material(null, materialGreenJade,         "Green Jade",       "", null, false, null, null, 0);
	materials[materialMetal]            = new Material(null, materialMetal,             "Metal",            "", null, false, null, null, 0);
	materials[materialObsidian]         = new Material(null, materialObsidian,          "Obsidian",         "", null, false, null, null, 0);
	materials[materialOnyx]             = new Material(null, materialOnyx,              "Onyx",             "", null, false, null, null, 0);
	materials[materialSerpentine]       = new Material(null, materialSerpentine,        "Serpentine",       "", null, false, null, null, 0);
	materials[materialStone]            = new Material(null, materialStone,             "Stone",            "", null, false, null, null, 0);
	materials[materialTigerEye]         = new Material(null, materialTigerEye,          "Tiger Eye",        "", null, false, null, null, 0);
	materials[materialTourmaline]       = new Material(null, materialTourmaline,        "Tourmaline",       "", null, false, null, null, 0);
	materials[materialTurquoise]        = new Material(null, materialTurquoise,         "Turquoise",        "", null, false, null, null, 0);
	materials[materialWhiteJade]        = new Material(null, materialWhiteJade,         "White Jade",       "", null, false, null, null, 0);
	materials[materialWhiteQuartz]      = new Material(null, materialWhiteQuartz,       "White Quartz",     "", null, false, null, null, 0);
	materials[materialWood]             = new Material(null, materialWood,              "Wood",             "", null, false, null, null, 0);
	materials[materialYellowGarnet]     = new Material(null, materialYellowGarnet,      "Yellow Garnet",    "", null, false, null, null, 0);
	
	core.getMaterial = function getMaterial(material) {
		return materials[material];
	};
	
	core.getMaterials = function getMaterials() {
		return materials;
	};
	
	function fGetMinDamage(maxDamage, variance) {
		var minDamage = maxDamage - (variance * maxDamage);
		return round(minDamage, 2);
	};
	
	core.getOptimalIronGraniteOrder = function getOptimalIronGraniteOrder(aco) {
		var order = [];

		var bloodDrinker = 22;// Assume we're going to be casting lvl 7 blood drinker (+22 dmg) 

		var lvlWieldReq = getObjectValue(aco, "oai.ibi.lvlWieldReq", 0);

		var buffBonus = bloodDrinker + (lvlWieldReq/20);

		var mod = 2;
		var rate = 0.1;
		
		var spells = getAcoSpells({aco:aco});
		var spellid, szName;
		for (var i = 0; i < spells.length; i++) {
			spellid = spells[i];
			szName = getSpellName(spellid);
			if (szName.match("Legendary Blood Thirst")){
				buffBonus +=10;	// I'm guessing it's 10, it's not on the wiki.
			} else if (szName.match("Epic Blood Thirst")) {
				buffBonus +=7;
			} else if (szName.match("Major Blood Thirst")) {
				buffBonus +=4;
			} else if (szName.match("Minor Blood Thirst")) {
				buffBonus +=2;
			}
		}

		var dhealth = getObjectValue(aco, "oai.iwi.dhealth", 0);

		var maxdamage = dhealth + buffBonus;
		var variance = getObjectValue(aco, "oai.iwi.scaleDamageRange");

		var bGranite=0;
		var bIron=0;
		var tinks = 0;
		
		var maxdamageI, maxdamageG;
		var varianceI, varianceG;
		var mindamageI, mindamageG;
		var entry;
		while (tinks <= 100) {
			maxdamageI = maxdamage + 1;
			maxdamageG = maxdamage;
			
			varianceI = variance;
			varianceG = 0.8 * variance;
			
			mindamageI = fGetMinDamage(maxdamageI, varianceI);
			mindamageG = fGetMinDamage(maxdamageG, varianceG);
			
			var meanI = (mindamageI + maxdamageI) / 2;
			var meanG = (mindamageG + maxdamageG) / 2;
			
			var critI = maxdamageI * mod;
			var critG = maxdamageG * mod;
			
			var averageI = (rate * critI) + ((1 - rate) * meanI);
			var averageG = (rate * critG) + ((1 - rate) * meanG);

			entry = {};
			entry.variance = varianceG;
			entry.maxdamage = maxdamageI;
			entry.baseMaxDamage = maxdamageI - buffBonus;

			if (averageG > averageI) {
				bGranite++;
				variance = varianceG;

				entry.material = materialGranite;
				order.push(entry);
			} else {
				bIron++;
				maxdamage = maxdamageI;

				entry.material = materialIron;
				order.push(entry);
			}
			tinks++;
		}
		return order;
	};
	
	core.getFullAcoName = function getFullAcoName(aco) {
		// Return a aco's name with its material name.
		var szName = "";
		if (aco) {
			szName = aco.szName;
			if (aco.material > 0) {
				if (materials[aco.material]) {
					szName = materials[aco.material].name + " " + aco.szName;
				}
			}
		}
		return szName;
	};
	
	function getAcoSpells(params) {
		var aco = params.aco;
		var spells = [];
		if (typeof aco["oai.iei.cspellid"] !== "undefined") {
			for (var key in aco)  {
				if (!aco.hasOwnProperty(key)) continue;
				if (key.substring(0, 18) == "oai.iei.rgspellid.") {
					spells.push( Number(key.substring(18, key.length)) );
				}
			}
		} else {
			if (aco.oai && aco.oai.iei && aco.oai.iei.cspellid > 0) {
				for (var i = 0; i < aco.oai.iei.cspellid; i++) {
					spells.push( aco.oai.iei.rgspellid(i) );
				}
			}
		}
		return spells;
	};

	/**
	 * getObjectValue() Get object value at a given path. Supports normal and flattened aco objects.
	 *
	 * @param {aco} target: aco object.
	 * @param {string} path: Variable path.
	 * @return {variable} Value from the path.
	 */
	core.getObjectValue = function getObjectValue(target, path, def) {
		if (typeof target[path] !== "undefined") {
			return target[path];
		}
		
		var segs = path.split(".");
		var len = segs.length;
		var idx = 0;
		
		var value;
		do {
			var prop = segs[idx];
			if (typeof prop === 'number') {
				prop = String(prop);
			}
			
			try {
				value = target[prop]
			} catch (e) {
				break;
			}

			if (typeof prop === "undefined") break;
			
			target = target[prop];
		} while (++idx < len && typeof target !== "undefined");
		
		if (idx === len) {
			return target;
		}
		
		return def;
	};
	
	return core;
}));